<?php
/**
 * Created by PhpStorm.
 * User: darksoldiers
 * Date: 23/10/2015
 * Time: 0:35
 */

?>
<body class="tm-background">

<nav class="tm-navbar uk-navbar uk-navbar-attached">
    <div class="uk-container uk-container-center">

        <a class="uk-navbar-brand uk-hidden-small" href="../index.html"><?php echo $text['web']; ?></a>

        <ul class="uk-navbar-nav uk-hidden-small">
            <li><a href="">Inicio</a></li>
            <li><a href="">Prensa</a></li>
            <li><a href="">Precios</a></li>
            <li><a href="">Comunidad</a></li>
            <li><a href="">Registro</a></li>
            <li><a href="">Conectarse</a></li>
        </ul>

        <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

        <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><?php echo $text['web'] ?></div>

    </div>
</nav>


<div class="uk-block uk-block-large">

    <div class="uk-container">

        <h3>World of Gestión - ¿Que es y para que sirve?</h3>

        <div class="uk-grid uk-grid-match" data-uk-grid-margin>
            <div class="uk-width-medium-1-3">
                <div class="uk-panel">
                    <p>Nacemos de la idea firme de crear un mercado virtual y activo para diferentes juegos en linea que día
                        a día salen al mercado.</p>
                </div>
            </div>
            <div class="uk-width-medium-1-3">
                <div class="uk-panel">
                    <p>Tendras el control entero de tu puesto de "mercado" y tendras todas las funciones como si fueras en realidad
                        un mercader con una completa gestión de tu negocio.</p>
                </div>
            </div>
            <div class="uk-width-medium-1-3">
                <div class="uk-panel">
                    <p>Damos soporte desde servidores oficiales hasta servidores no-oficiales, en caso de que un servidor sea custom
                        la plataforma no esta adaptada para esos tipos de servidores.</p>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="tm-section tm-section-color-white">
    <div class="uk-container uk-container-center uk-text-center">

        <h1 class="tm-margin-large-bottom"><strong>Involúcrate .</strong> Conviertete en parte de la comunidad.</h1>

        <div class="uk-grid tm-grid-margin-large" data-uk-grid-match="{target:'.uk-panel'}" data-uk-grid-margin>

            <div class="uk-width-medium-1-3">
                <a class="uk-panel tm-panel-link" href="#">
                    <div class="tm-icon"><i class="uk-icon-twitter uk-icon-large"></i></div>
                    <h2>Twitter</h2>
                    <p> Síguenos en Twitter y sé el primero en recibir noticias y actualizaciones. Estamos abiertos a sus comentarios y preguntas.</p>
                </a>
            </div>

            <div class="uk-width-medium-1-3">
                <a class="uk-panel tm-panel-link" href="">
                    <div class="tm-icon"><i class="uk-icon-facebook uk-icon-large"></i></div>
                    <h2>Facebook</h2>
                    <p> Síguenos en Facebook y sé el primero en recibir noticias y actualizaciones. Estamos abiertos a sus comentarios y preguntas.</p>
                </a>
            </div>

            <div class="uk-width-medium-1-3">
                <a class="uk-panel tm-panel-link" href="#">
                    <div class="tm-icon"><i class="uk-icon-google-plus uk-icon-large"></i></div>
                    <h2>Google+ Comunidad</h2>
                    <p>Si usted está interesado en discutir, participar y ayudar a los demás, la comunidad de Google+ es el lugar para estar.</p>
                </a>
            </div>

        </div>

    </div>
</div>




    <div class="tm-footer">

        <div class="uk-container uk-container-center uk-text-center">

            <div class="uk-panel">
                <p>Todos los derechos reservados a World of Gestión. World of Gestión CMS <strong><a href=""> 2.0 </a></strong></p>
            </div>

            <ul class="uk-subnav uk-subnav-line uk-flex-center">
                <li><a href="">Patrocinadores</a></li>
                <li><a href="">Interrupciones del servicio</a> </li>
                <li><a href="">Changelog</a></li>
            </ul>



        </div>

   </div>

    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
    <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/6035/jquery.flexslider-min.js'></script>

    <script src="js/index.js"></script>

</div>
</body>