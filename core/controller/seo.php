<?php
/*
* Nombre : Modulo de SEO (Navegacion)
* Autor : Ares
* Versión : 0.1Alpha
* Web : Reignofnorrath.es
 */
$page = $_GET['p'];
if ($page == "")
{
    @include('app/themes/default/pages/index.inc');
}
switch($page)
{
    case "index":
        @include('app/themes/default/pages/index.inc');
        break;
    case "blog":
        @include('app/themes/default/pages/blog/view.inc');
        break;
    case "noticia":
        @include('app/themes/default/pages/blog/noticia.inc');
        break;
    default:
        @include('');
}