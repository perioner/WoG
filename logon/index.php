<?
/**
* Index World of Gestión
* v0.2
* Proximamente !
*/
header("Content-Type: text/html;charset=utf-8");
include('../core/controller/settings.php');


?>


<!DOCTYPE html>
<html>

<head>


  <title><?php echo $text['web']; ?></title>

<!-- Integración Uikit -->

	<link rel="stylesheet" href="../app/themes/default/css/home.css" type="text/css" />
	<link rel="stylesheet" href="../app/themes/default/css/home_min.css" type="text/css" />

<!-- Integración BOOSTRAP -->

	<link rel="stylesheet" href="../app/themes/default/css/bootstrap-responsive.css" type="text/css" />
	<link rel="stylesheet" href="../app/themes/default/css/bootstrap.css" type="text/css" />

<!-- OLD SOURCE -->

<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>
<link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/6035/grid.css'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600'>
<link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/6035/flexslider.css'>
<link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/6035/icomoon-uikit-feb.css'>
    <script src="js/prefixfree.min.js"></script>

</head>

<body>

    <div class="navbar navbar-inverse">
      <div class="navbar-inner">
        <div class="container">
     
          <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#"><?php echo $text['web']; ?></a>
     
          <!-- Everything you want hidden at 940px or less, place within here -->
          <div class="nav-collapse collapse">
                <ul class="nav nav-pills">
      				<li class="active">
        				<a href="#">Inicio</a>
     				 </li>
      				<li><a href="#">Prensa</a></li>
     				<li><a href="#">Foros de Soporte</a></li>
     				<li><a href="#">Registro</a></li>
     				<li><a href="#">Area de usuarío</a></li>
    			</ul>
          </div>
     
        </div>
      </div>
    </div>


<div class="container">

<div class="grid_12">
  <?php if($web['mantenaince']) : ?>
    <div class="alert alert-danger">
      <span class="icon-checkmark"></span>
      Actualmente estamos teniendo problemas, con nuestros servicios.
    </div>
<?php endif; ?>


